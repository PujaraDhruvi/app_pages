import 'package:flutter/material.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[100],
        body: Container(
            padding: const EdgeInsets.fromLTRB(20, 50, 20, 20),
            child: Column(children: [
              Column(
                children: const [
                  Center(
                    child: CircleAvatar(
                      backgroundImage: NetworkImage("assets/images/1.jpg"),
                      radius: 55,
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Container(
                    margin: const EdgeInsets.fromLTRB(10, 10, 10, 3),
                    child: const Center(
                      child: Text(
                        'Gowtham Raj',
                        style: TextStyle(fontSize: 20),
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Container(
                    margin: const EdgeInsets.only(bottom: 20),
                    child: const Center(
                      child: Text(
                        'UI/UX Designer',
                        style: TextStyle(fontSize: 14, color: Colors.grey),
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Row(
                    children: [
                      Expanded(
                        flex: 4,
                        child: Container(
                          padding: const EdgeInsets.only(bottom: 5),
                          child: const Text(
                            'Profile Completeness',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                      const Text(
                        '87%',
                        textAlign: TextAlign.right,
                        style: TextStyle(
                          color: Colors.grey,
                        ),
                      ),
                    ],
                  ),
                  const LinearProgressIndicator(
                    value: 0.77,
                    minHeight: 7.0,
                    color: Colors.green,
                    backgroundColor: Colors.white38, // <-- SEE HERE
                  ),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    padding: const EdgeInsets.only(top: 20),
                    child: Row(
                      children: const [
                        Expanded(
                          flex: 4,
                          child: Text(
                            'Recommended Jobs',
                            textAlign: TextAlign.left,
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          ),
                        ),
                        Text(
                          'SEE ALL',
                          textAlign: TextAlign.right,
                          style: TextStyle(
                            color: Colors.blueAccent,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    Stack(
                      children: [
                        Container(
                          padding: const EdgeInsets.fromLTRB(0, 15, 10, 10),
                          height: 280,
                          width: 250,
                          child: Card(
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(20),
                              child: const Image(
                                image: NetworkImage('assets/images/purple.jpg'),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 30, vertical: 40),
                          height: 280,
                          width: 250,
                          color: Colors.transparent,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text(
                                "Senior ui/ux\ndesigner",
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.white,
                                ),
                              ),
                              Text(
                                '\nmicrosoft corp.\n',
                                style: TextStyle(
                                  fontSize: 10,
                                  color: Colors.white,
                                ),
                              ),
                              Expanded(
                                flex: 4,
                                child: Text(
                                  'Delhi',
                                  style: TextStyle(
                                    fontSize: 10,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Stack(
                      children: [
                        Container(
                          padding: const EdgeInsets.fromLTRB(0, 15, 10, 10),
                          height: 280,
                          width: 250,
                          child: Card(
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(20),
                              child: const Image(
                                image: NetworkImage('assets/images/pink.jpg'),
                                fit: BoxFit.fill,
                              ),
                            ),
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 30, vertical: 40),
                          height: 280,
                          width: 250,
                          color: Colors.transparent,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: const [
                              Text(
                                "Senior ui/ux\ndesigner",
                                style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.white,
                                ),
                              ),
                              Text(
                                '\nmicrosoft corp.\n',
                                style: TextStyle(
                                  fontSize: 10,
                                  color: Colors.white,
                                ),
                              ),
                              Expanded(
                                flex: 4,
                                child: Text(
                                  'Delhi',
                                  style: TextStyle(
                                    fontSize: 10,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    padding: const EdgeInsets.only(top: 20),
                    child: Row(
                      children: const [
                        Expanded(
                          flex: 4,
                          child: Text(
                            'Recent Activities',
                            textAlign: TextAlign.left,
                            style: TextStyle(color: Colors.black, fontSize: 20),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Row(
                children: [
                  Column(children: [
                    Container(
                        padding: const EdgeInsets.fromLTRB(0, 15, 10, 0),
                        width: 175,
                        height: 100,
                        child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                            ),
                            color: Colors.white,
                            elevation: 10,
                            child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: const <Widget>[
                                  ListTile(
                                    leading: Icon(
                                      shadows: [
                                        BoxShadow(
                                          blurRadius: 30.0,
                                          color: Colors.pinkAccent,
                                        ),
                                      ],
                                      Icons.bookmark,
                                      size: 40,
                                      color: Colors.pinkAccent,
                                    ),
                                    title: Text('49',
                                        style: TextStyle(fontSize: 25.0)),
                                    subtitle: Text('Saved Jobs',
                                        style: TextStyle(fontSize: 13.0)),
                                  ),
                                ])))
                  ]),
                  Column(children: [
                    Container(
                        padding: const EdgeInsets.only(top: 15),
                        width: 175,
                        height: 100,
                        child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                            ),
                            color: Colors.white,
                            elevation: 10,
                            child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: const <Widget>[
                                  ListTile(
                                    leading: Icon(
                                      shadows: [
                                        BoxShadow(
                                          blurRadius: 30.0,
                                          color: Colors.purpleAccent,
                                        ),
                                      ],
                                      Icons.beenhere,
                                      size: 40,
                                      color: Colors.purple,
                                    ),
                                    title: Text('124',
                                        style: TextStyle(fontSize: 25.0)),
                                    subtitle: Text('Applied',
                                        style: TextStyle(fontSize: 13.0)),
                                  ),
                                ])))
                  ]),
                ],
              )
            ])));
  }
}

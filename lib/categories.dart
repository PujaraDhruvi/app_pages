import 'package:flutter/material.dart';

class Categories extends StatelessWidget {
  const Categories({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[200],
        appBar: AppBar(
          toolbarHeight: 100,
          backgroundColor: Colors.white,
          title: Row(
            children: [
              const Expanded(
                flex: 4,
                child: Text(
                  'Categories',
                  textAlign: TextAlign.left,
                  style: TextStyle(color: Colors.black, fontSize: 30),
                ),
              ),
              ElevatedButton(
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.red,
                  shape: const CircleBorder(),
                  padding: const EdgeInsets.all(12),
                ),
                child: const Text('OS'),
              ),
              const Expanded(
                child: CircleAvatar(
                  radius: 25,
                  backgroundImage: NetworkImage(
                    'assets/images/1.jpg',
                  ),
                ),
              ),
            ],
          ),
        ),
        body: Center(
            child: GridView.count(crossAxisCount: 2, children: [
          Container(
            margin: const EdgeInsets.fromLTRB(10, 10, 0, 5),
            color: Colors.white10,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Column(children: [
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          margin: const EdgeInsets.all(10.0),
                          child: const Icon(
                            Icons.design_services_sharp,
                            color: Colors.blue,
                            size: 60.0,
                            shadows: [
                              BoxShadow(
                                blurRadius: 20.0,
                                color: Colors.blueAccent,
                              ),
                              BoxShadow(
                                blurRadius: 10.0,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                Column(
                  children: [
                    Center(
                        child: Container(
                      height: 70,
                      width: 200,
                      margin: const EdgeInsets.fromLTRB(10, 10, 10.0, 10.0),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(
                          Radius.circular(10),
                        ),
                        color: Colors.grey[200],
                      ),
                      child: Column(children: const <Widget>[
                        ListTile(
                          title:
                              Text('Design', style: TextStyle(fontSize: 22.0)),
                          subtitle: Text('6000+ jobs',
                              style: TextStyle(
                                fontSize: 20.0,
                              )),
                        )
                      ]),
                    )),
                  ],
                )
              ]),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(10, 10, 10, 5),
            color: Colors.white10,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Column(children: [
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          margin: const EdgeInsets.all(10.0),
                          child: const Icon(
                            Icons.lan_outlined,
                            color: Colors.green,
                            size: 60.0,
                            shadows: [
                              BoxShadow(
                                blurRadius: 20.0,
                                color: Colors.greenAccent,
                              ),
                              BoxShadow(
                                blurRadius: 10.0,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                Column(
                  children: [
                    Center(
                        child: Container(
                      height: 70,
                      width: 200,
                      margin: const EdgeInsets.fromLTRB(10, 10, 10.0, 10.0),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(
                          Radius.circular(10),
                        ),
                        color: Colors.grey[200],
                      ),
                      child: Column(children: const <Widget>[
                        ListTile(
                          title: Text('IT', style: TextStyle(fontSize: 22.0)),
                          subtitle: Text('8000+ jobs',
                              style: TextStyle(
                                fontSize: 20.0,
                              )),
                        )
                      ]),
                    )),
                  ],
                )
              ]),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(10, 10, 0, 5),
            color: Colors.white10,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Column(children: [
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          margin: const EdgeInsets.all(10.0),
                          child: const Icon(
                            Icons.pie_chart,
                            color: Colors.orange,
                            size: 60.0,
                            shadows: [
                              BoxShadow(
                                blurRadius: 20.0,
                                color: Colors.orangeAccent,
                              ),
                              BoxShadow(
                                blurRadius: 10.0,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                Column(
                  children: [
                    Center(
                        child: Container(
                      height: 70,
                      width: 200,
                      margin: const EdgeInsets.fromLTRB(10, 10, 10.0, 10.0),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(
                          Radius.circular(10),
                        ),
                        color: Colors.grey[200],
                      ),
                      child: Column(children: const <Widget>[
                        ListTile(
                          title: Text('Marketing',
                              style: TextStyle(fontSize: 22.0)),
                          subtitle: Text('6000+ jobs',
                              style: TextStyle(
                                fontSize: 20.0,
                              )),
                        )
                      ]),
                    )),
                  ],
                )
              ]),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(10, 10, 10, 5),
            color: Colors.white10,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Column(children: [
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          margin: const EdgeInsets.all(10.0),
                          child: const Icon(
                            Icons.menu_book_outlined,
                            color: Colors.red,
                            size: 60.0,
                            shadows: [
                              BoxShadow(
                                blurRadius: 20.0,
                                color: Colors.redAccent,
                              ),
                              BoxShadow(
                                blurRadius: 10.0,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                Column(
                  children: [
                    Center(
                        child: Container(
                      height: 70,
                      width: 200,
                      margin: const EdgeInsets.fromLTRB(10, 10, 10.0, 10.0),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(
                          Radius.circular(10),
                        ),
                        color: Colors.grey[200],
                      ),
                      child: Column(children: const <Widget>[
                        ListTile(
                          title: Text('Teaching',
                              style: TextStyle(fontSize: 22.0)),
                          subtitle: Text('8000+ jobs',
                              style: TextStyle(
                                fontSize: 20.0,
                              )),
                        )
                      ]),
                    )),
                  ],
                )
              ]),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(10, 10, 0, 5),
            color: Colors.white10,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Column(children: [
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          margin: const EdgeInsets.all(10.0),
                          child: const Icon(
                            Icons.engineering_sharp,
                            color: Colors.purple,
                            size: 60.0,
                            shadows: [
                              BoxShadow(
                                blurRadius: 20.0,
                                color: Colors.purpleAccent,
                              ),
                              BoxShadow(
                                blurRadius: 10.0,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                Column(
                  children: [
                    Center(
                        child: Container(
                      height: 70,
                      width: 200,
                      margin: const EdgeInsets.fromLTRB(10, 10, 10.0, 10.0),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(
                          Radius.circular(10),
                        ),
                        color: Colors.grey[200],
                      ),
                      child: Column(children: const <Widget>[
                        ListTile(
                          title: Text('Engineering',
                              style: TextStyle(fontSize: 20.0)),
                          subtitle: Text('8000+ jobs',
                              style: TextStyle(
                                fontSize: 20.0,
                              )),
                        )
                      ]),
                    )),
                  ],
                )
              ]),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(10, 10, 10, 5),
            color: Colors.white10,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Column(children: [
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          margin: const EdgeInsets.all(10.0),
                          child: const Icon(
                            Icons.medical_services_rounded,
                            color: Colors.pink,
                            size: 60.0,
                            shadows: [
                              BoxShadow(
                                blurRadius: 20.0,
                                color: Colors.pinkAccent,
                              ),
                              BoxShadow(
                                blurRadius: 10.0,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                Column(
                  children: [
                    Center(
                        child: Container(
                      height: 70,
                      width: 200,
                      margin: const EdgeInsets.fromLTRB(10, 10, 10.0, 10.0),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(
                          Radius.circular(10),
                        ),
                        color: Colors.grey[200],
                      ),
                      child: Column(children: const <Widget>[
                        ListTile(
                          title:
                              Text('Medical', style: TextStyle(fontSize: 22.0)),
                          subtitle: Text('9000+ jobs',
                              style: TextStyle(
                                fontSize: 20.0,
                              )),
                        )
                      ]),
                    )),
                  ],
                )
              ]),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(10, 10, 0, 5),
            color: Colors.white10,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Column(children: [
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          margin: const EdgeInsets.all(10.0),
                          child: const Icon(
                            Icons.mediation_sharp,
                            color: Colors.blue,
                            size: 60.0,
                            shadows: [
                              BoxShadow(
                                blurRadius: 20.0,
                                color: Colors.blueAccent,
                              ),
                              BoxShadow(
                                blurRadius: 10.0,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                Column(
                  children: [
                    Center(
                        child: Container(
                      height: 70,
                      width: 200,
                      margin: const EdgeInsets.fromLTRB(10, 10, 10, 10.0),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(
                          Radius.circular(10),
                        ),
                        color: Colors.grey[200],
                      ),
                      child: Column(children: const <Widget>[
                        ListTile(
                          title: Text('Networking',
                              style: TextStyle(fontSize: 20.5)),
                          subtitle: Text('4000+ jobs',
                              style: TextStyle(
                                fontSize: 20.0,
                              )),
                        )
                      ]),
                    )),
                  ],
                )
              ]),
            ),
          ),
          Container(
            margin: const EdgeInsets.fromLTRB(10, 10, 10, 5),
            color: Colors.white10,
            child: Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              child: Column(children: [
                Column(
                  children: [
                    Row(
                      children: [
                        Container(
                          margin: const EdgeInsets.all(10.0),
                          child: const Icon(
                            Icons.video_camera_back_rounded,
                            color: Colors.green,
                            size: 60.0,
                            shadows: [
                              BoxShadow(
                                blurRadius: 20.0,
                                color: Colors.greenAccent,
                              ),
                              BoxShadow(
                                blurRadius: 10.0,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                Column(
                  children: [
                    Center(
                        child: Container(
                      height: 70,
                      width: 200,
                      margin: const EdgeInsets.fromLTRB(10, 10, 10.0, 10.0),
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.all(
                          Radius.circular(10),
                        ),
                        color: Colors.grey[200],
                      ),
                      child: Column(children: const <Widget>[
                        ListTile(
                          title: Text('Shooting',
                              style: TextStyle(fontSize: 22.0)),
                          subtitle: Text('4000+ jobs',
                              style: TextStyle(
                                fontSize: 20.0,
                              )),
                        )
                      ]),
                    )),
                  ],
                )
              ]),
            ),
          ),
        ])));
  }
}

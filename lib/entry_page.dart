import 'package:app_pages/categories.dart';
import 'package:app_pages/design.dart';
import 'package:app_pages/search.dart';
import 'package:app_pages/user_list.dart';
import 'package:flutter/material.dart';
import 'package:app_pages/home.dart';

class EntryPage extends StatefulWidget {
  const EntryPage({super.key});

  @override
  _EntryPageState createState() => _EntryPageState();
}

class _EntryPageState extends State<EntryPage> {
  int _currentIndex = 0;
  final List _screens = [
    const Home(),
    const Design(),
    const Categories(),
    const UserListPage(),
    const Search(),
  ];

  void _updateIndex(int value) {
    setState(() {
      _currentIndex = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[100],
        body: _screens[_currentIndex],
        bottomNavigationBar: ClipRRect(
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(30.0),
            topRight: Radius.circular(30.0),
          ),
          child: BottomNavigationBar(
            backgroundColor: Colors.white,
            elevation: 10.0,
            type: BottomNavigationBarType.fixed,
            currentIndex: _currentIndex,
            onTap: _updateIndex,
            selectedItemColor: Colors.blue[700],
            selectedFontSize: 17,
            unselectedFontSize: 17,
            iconSize: 35,
            items: const [
              BottomNavigationBarItem(
                label: "Home",
                icon: Icon(Icons.home_filled),
              ),
              BottomNavigationBarItem(
                label: "Design",
                icon: Icon(Icons.design_services_sharp),
              ),
              BottomNavigationBarItem(
                label: "Categories",
                icon: Icon(Icons.category),
              ),
              BottomNavigationBarItem(
                label: "Connections",
                icon: Icon(Icons.groups_rounded),
              ),
              BottomNavigationBarItem(
                label: "Search",
                icon: Icon(Icons.search_outlined),
              ),
            ],
          ),
        ));
  }
}

import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

import 'add_Connection_page.dart';

class UserListPage extends StatefulWidget {
  const UserListPage({super.key});

  @override
  State<UserListPage> createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
      appBar: AppBar(
        toolbarHeight: 100,
        backgroundColor: Colors.white,
        title: Row(
          children: [
            const Expanded(
              flex: 4,
              child: Text(
                'Connections',
                textAlign: TextAlign.left,
                style: TextStyle(color: Colors.black, fontSize: 30),
              ),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: ((context) {
                    return AddConnectionName(null);
                  }),
                )).then(((value) {
                  setState(() {
                    getUser();
                  });
                }));
              },
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.red,
                shape: const CircleBorder(),
                padding: const EdgeInsets.all(12),
              ),
              child: const Icon(Icons.add),
            ),
            const Expanded(
              child: CircleAvatar(
                radius: 25,
                backgroundImage: NetworkImage(
                  'assets/images/1.jpg',
                ),
              ),
            ),
          ],
        ),
      ),
      body: FutureBuilder<http.Response>(
        builder: (context, snapshot) {
          if (snapshot.data != null && snapshot.hasData) {
            return ListView.builder(
              itemCount: jsonDecode(snapshot.data!.body.toString()).length,
              itemBuilder: ((context, index) {
                return InkWell(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) {
                        return AddConnectionName(
                            jsonDecode(snapshot.data!.body.toString())[index]);
                      },
                    )).then(
                      (value) {
                        setState(() {
                          getUser();
                        });
                      },
                    );
                  },
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                (jsonDecode(snapshot.data!.body.toString())[
                                        index]['name'])
                                    .toString(),
                                style: const TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold),
                              ),
                              Text(
                                (jsonDecode(snapshot.data!.body.toString())[
                                        index]['salary'])
                                    .toString(),
                                style: TextStyle(
                                    fontSize: 13, color: Colors.grey.shade600),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              Container(
                                padding: const EdgeInsets.only(right: 20),
                                child: InkWell(
                                  onTap: () {
                                    alert(jsonDecode(snapshot.data!.body
                                        .toString())[index]['id']);
                                  },
                                  child: const Icon(
                                    Icons.delete,
                                    color: Colors.red,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              }),
            );
          } else {
            return const Center(child: CircularProgressIndicator());
          }
        },
        future: getUser(),
      ),
    ));
  }

  void alert(id) {
    showDialog(
      context: context,
      builder: ((context) {
        return AlertDialog(
          title: const Text('Delete'),
          content: const Text('Are you Sure Want To Delete ?'),
          actions: [
            TextButton(
                onPressed: ((() async {
                  http.Response res = await deleteUser(id);
                  if (res.statusCode == 200) {
                    setState(() {});
                  }
                  Navigator.of(context).pop();
                })),
                child: const Text('Yes')),
            TextButton(
                onPressed: ((() {
                  Navigator.of(context).pop();
                })),
                child: const Text('No')),
          ],
        );
      }),
    );
  }

  Future<http.Response> getUser() async {
    var res = await http
        .get(Uri.parse('https://64056e6eeed195a99f827c8f.mockapi.io/users'));
    return res;
  }

  Future<http.Response> deleteUser(id) async {
    // print(id);
    var response1 = await http.delete(
        Uri.parse("https://64056e6eeed195a99f827c8f.mockapi.io/users/" + id));
    return response1;
  }
}

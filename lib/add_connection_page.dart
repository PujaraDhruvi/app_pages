import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class AddConnectionName extends StatefulWidget {
  @override
  State<AddConnectionName> createState() => _AddConnectionNameState();

  dynamic map;
  AddConnectionName(this.map, {super.key});

  GlobalKey<FormState> formkey = GlobalKey();
  var name = TextEditingController();
  var salary = TextEditingController();
}

class _AddConnectionNameState extends State<AddConnectionName> {
  @override
  void initState() {
    super.initState();
    widget.name.text = widget.map == null ? '' : widget.map['name'];
    widget.salary.text = widget.map == null ? '' : widget.map['salary'];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          TextFormField(
            controller: widget.name,
            decoration: const InputDecoration(
              hintText: "Enter Employee's name",
            ),
          ),
          TextFormField(
            decoration: const InputDecoration(
              hintText: "Enter Employee's salary",
            ),
            controller: widget.salary,
          ),
          Container(
            margin: const EdgeInsets.only(top: 20),
            child: ElevatedButton(
              onPressed: () async {
                if (widget.map == null) {
                  await addEmployee().then((value) => (value) {});
                } else {
                  await editEmployee().then((value) => (value) {
                        setState(() {});
                      });
                }

                Navigator.of(context).pop(true);
              },
              child: const Text(
                "OK",
                style: TextStyle(fontSize: 24),
              ),
            ),
          )
        ],
      ),
    );
  }

  Future<void> addEmployee() async {
    var map = {};
    map['name'] = widget.name.text;
    map['salary'] = widget.salary.text;
    var response1 = http.post(
        Uri.parse(
          "https://64056e6eeed195a99f827c8f.mockapi.io/users",
        ),
        body: map);
  }

  Future<void> editEmployee() async {
    var map = {};
    map['name'] = widget.name.text;
    map['salary'] = widget.salary.text;
    var response1 = http.put(
        Uri.parse(
          "https://64056e6eeed195a99f827c8f.mockapi.io/users/${widget.map['id']}",
        ),
        body: map);
  }
}

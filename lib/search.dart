import 'package:flutter/material.dart';

class Search extends StatelessWidget {
  const Search({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        toolbarHeight: 200,
        backgroundColor: Colors.white,
        title: Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.fromLTRB(0, 0, 10, 60),
                    child: const Text(
                      'Search',
                      textAlign: TextAlign.left,
                      style: TextStyle(color: Colors.black, fontSize: 30),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              children: const [
                Expanded(
                  child: Text(
                    'Keywords, Designations, Companies',
                    textAlign: TextAlign.left,
                    style: TextStyle(color: Colors.grey, fontSize: 15),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      body: Column(
        children: [
          Column(
            children: [
              Container(
                padding: const EdgeInsets.all(15),
                child: Row(
                  children: const [
                    Expanded(
                      flex: 4,
                      child: Text(
                        'Location',
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.black, fontSize: 20),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Row(
              children: [
                Row(children: [
                  Container(
                      padding: const EdgeInsets.fromLTRB(15, 0, 15, 15),
                      width: 140,
                      height: 70,
                      child: SizedBox(
                        child: InkWell(
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: const BorderSide(
                                color: Colors.blue,
                              ),
                            ),
                            color: Colors.blue[900],
                            elevation: 10,
                            child: const Center(
                                child: Text(
                              'Chennai',
                              style: TextStyle(color: Colors.white),
                            )),
                          ),
                          onTap: () {},
                        ),
                      ))
                ]),
                Row(children: [
                  Container(
                      padding: const EdgeInsets.fromLTRB(0, 0, 15, 15),
                      width: 140,
                      height: 70,
                      child: SizedBox(
                        child: InkWell(
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: const BorderSide(
                                color: Colors.blue,
                              ),
                            ),
                            color: Colors.blue[50],
                            elevation: 10,
                            child: Center(
                                child: Text(
                              'Delhi',
                              style: TextStyle(color: Colors.blue[900]),
                            )),
                          ),
                          onTap: () {},
                        ),
                      ))
                ]),
                Row(children: [
                  Container(
                      padding: const EdgeInsets.fromLTRB(0, 0, 15, 15),
                      width: 140,
                      height: 70,
                      child: SizedBox(
                        child: InkWell(
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: const BorderSide(
                                color: Colors.blue,
                              ),
                            ),
                            color: Colors.blue[50],
                            elevation: 10,
                            child: Center(
                                child: Text(
                              'Lucknow',
                              style: TextStyle(color: Colors.blue[900]),
                            )),
                          ),
                          onTap: () {},
                        ),
                      ))
                ]),
                Row(children: [
                  Container(
                      padding: const EdgeInsets.fromLTRB(0, 0, 15, 15),
                      width: 140,
                      height: 70,
                      child: SizedBox(
                        child: InkWell(
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: const BorderSide(
                                color: Colors.blue,
                              ),
                            ),
                            color: Colors.blue[50],
                            elevation: 10,
                            child: Center(
                                child: Text(
                              'Hydrabad',
                              style: TextStyle(color: Colors.blue[900]),
                            )),
                          ),
                          onTap: () {},
                        ),
                      ))
                ]),
                Row(children: [
                  Container(
                      padding: const EdgeInsets.fromLTRB(0, 0, 15, 15),
                      width: 140,
                      height: 70,
                      child: SizedBox(
                        child: InkWell(
                          child: Card(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: const BorderSide(
                                color: Colors.blue,
                              ),
                            ),
                            color: Colors.blue[50],
                            elevation: 10,
                            child: Center(
                                child: Text(
                              'Kanpur',
                              style: TextStyle(color: Colors.blue[900]),
                            )),
                          ),
                          onTap: () {},
                        ),
                      ))
                ])
              ],
            ),
          ),
          Column(
            children: [
              Container(
                padding: const EdgeInsets.all(15),
                child: Row(
                  children: const [
                    Expanded(
                      flex: 4,
                      child: Text(
                        'Others',
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.black, fontSize: 20),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          /*Column(
            children: [
              Container(
                padding: const EdgeInsets.all(50),
                child: Row(
                  children: [
                    Container(
                      margin: EdgeInsets.only(right: 15),
                      height: 250,
                      width: 80,
                      decoration: const BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(40))),
                      child: const RotatedBox(
                        quarterTurns: -1,
                        child: Text(
                          'Salary Representation',
                          style: TextStyle(fontSize: 50),
                          child: LinearProgressIndicator(
                            backgroundColor: Colors.white54,
                            value: 0.4,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 15),
                      height: 250,
                      width: 80,
                      decoration: const BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.all(Radius.circular(40))),
                      child: const RotatedBox(
                        quarterTurns: -1,
                        child: LinearProgressIndicator(
                          value: 0.4,
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(right: 15),
                      height: 250,
                      width: 80,
                      decoration: const BoxDecoration(
                          color: Colors.blue,
                          borderRadius: BorderRadius.all(Radius.circular(40))),
                      child: const RotatedBox(
                        quarterTurns: -1,
                        child: LinearProgressIndicator(
                          value: 0.4,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),*/
        ],
      ),
    );
  }
}

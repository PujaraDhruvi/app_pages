import 'package:flutter/material.dart';

class Design extends StatelessWidget {
  const Design({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[50],
        appBar: AppBar(
          toolbarHeight: 100,
          backgroundColor: Colors.white,
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios_rounded, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Row(
            children: [
              const Expanded(
                flex: 4,
                child: Text(
                  'Design',
                  textAlign: TextAlign.left,
                  style: TextStyle(color: Colors.black, fontSize: 30),
                ),
              ),
              ElevatedButton(
                onPressed: () {},
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.red,
                  shape: const CircleBorder(),
                  padding: const EdgeInsets.all(12),
                ),
                child: const Text('OS'),
              ),
              const Expanded(
                child: CircleAvatar(
                  radius: 25,
                  backgroundImage: NetworkImage(
                    'assets/images/1.jpg',
                  ),
                ),
              ),
            ],
          ),
        ),
        body: SingleChildScrollView(
          child: Column(children: [
            Container(
              margin: const EdgeInsets.fromLTRB(12, 17, 12, 9),
              height: 190,
              child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: Colors.white,
                  elevation: 10,
                  child: Column(children: [
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.only(top: 10),
                          child: const ListTile(
                            leading: Icon(
                              Icons.computer,
                              size: 40,
                              color: Colors.blue,
                            ),
                            title: Text('UX Product Engineer',
                                style: TextStyle(fontSize: 23.0)),
                            subtitle: Text('Zoho corp',
                                style: TextStyle(fontSize: 17.0)),
                          ),
                        ),
                      ],
                    ),
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                      Container(
                        margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                        height: 100,
                        child: Card(
                          color: Colors.grey[50],
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Container(
                                    padding: const EdgeInsets.all(10),
                                    child: const Icon(
                                      Icons.location_on_sharp,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.all(10),
                                    child: const Text(
                                      'Chennai',
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.grey),
                                    ),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Container(
                                    padding: const EdgeInsets.fromLTRB(
                                        10, 8, 10, 10),
                                    child: const Icon(
                                      Icons.location_on_sharp,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.fromLTRB(
                                        10, 0, 10, 10),
                                    child: const Text(
                                      'Photoshop, Adobe XD, Android Studio',
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.grey),
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      )
                    ])
                  ])),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(12, 0, 12, 9),
              height: 190,
              child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: Colors.white,
                  elevation: 10,
                  child: Column(children: [
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.only(top: 10),
                          child: const ListTile(
                            leading: Icon(
                              Icons.g_mobiledata,
                              size: 60,
                              color: Colors.green,
                            ),
                            title: Text('Graphic Designer',
                                style: TextStyle(fontSize: 23.0)),
                            subtitle: Text('Google',
                                style: TextStyle(fontSize: 17.0)),
                          ),
                        ),
                      ],
                    ),
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                      Container(
                        margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                        height: 100,
                        child: Card(
                          color: Colors.grey[50],
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Container(
                                    padding: const EdgeInsets.all(10),
                                    child: const Icon(
                                      Icons.location_on_sharp,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.all(10),
                                    child: const Text(
                                      'Chennai',
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.grey),
                                    ),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Container(
                                    padding: const EdgeInsets.fromLTRB(
                                        10, 8, 10, 10),
                                    child: const Icon(
                                      Icons.location_on_sharp,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.fromLTRB(
                                        10, 0, 10, 10),
                                    child: const Text(
                                      'Photoshop, Adobe XD, Android Studio',
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.grey),
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      )
                    ])
                  ])),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(12, 0, 12, 9),
              height: 190,
              child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: Colors.white,
                  elevation: 10,
                  child: Column(children: [
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.only(top: 10),
                          child: const ListTile(
                            leading: Icon(
                              Icons.apple,
                              size: 40,
                              color: Colors.black,
                            ),
                            title: Text('Creative Director',
                                style: TextStyle(fontSize: 23.0)),
                            subtitle: Text(
                                'Samsung'
                                '',
                                style: TextStyle(fontSize: 17.0)),
                          ),
                        ),
                      ],
                    ),
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                      Container(
                        margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                        height: 100,
                        child: Card(
                          color: Colors.grey[50],
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Container(
                                    padding: const EdgeInsets.all(10),
                                    child: const Icon(
                                      Icons.location_on_sharp,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.all(10),
                                    child: const Text(
                                      'Chennai',
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.grey),
                                    ),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Container(
                                    padding: const EdgeInsets.fromLTRB(
                                        10, 8, 10, 10),
                                    child: const Icon(
                                      Icons.location_on_sharp,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.fromLTRB(
                                        10, 0, 10, 10),
                                    child: const Text(
                                      'Photoshop, Adobe XD, Android Studio',
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.grey),
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      )
                    ])
                  ])),
            ),
            Container(
              margin: const EdgeInsets.fromLTRB(12, 0, 12, 9),
              height: 190,
              child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  color: Colors.white,
                  elevation: 10,
                  child: Column(children: [
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Container(
                          padding: const EdgeInsets.only(top: 10),
                          child: const ListTile(
                            leading: Icon(
                              Icons.border_all_rounded,
                              size: 40,
                              color: Colors.yellow,
                            ),
                            title: Text('UI/UX Designer',
                                style: TextStyle(fontSize: 23.0)),
                            subtitle: Text('One Plus',
                                style: TextStyle(fontSize: 17.0)),
                          ),
                        ),
                      ],
                    ),
                    Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                      Container(
                        margin: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                        height: 100,
                        child: Card(
                          color: Colors.grey[50],
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Container(
                                    padding: const EdgeInsets.all(10),
                                    child: const Icon(
                                      Icons.location_on_sharp,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.all(10),
                                    child: const Text(
                                      'Chennai',
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.grey),
                                    ),
                                  )
                                ],
                              ),
                              Row(
                                children: [
                                  Container(
                                    padding: const EdgeInsets.fromLTRB(
                                        10, 8, 10, 10),
                                    child: const Icon(
                                      Icons.location_on_sharp,
                                      color: Colors.grey,
                                    ),
                                  ),
                                  Container(
                                    padding: const EdgeInsets.fromLTRB(
                                        10, 0, 10, 10),
                                    child: const Text(
                                      'Photoshop, Adobe XD, Android Studio',
                                      style: TextStyle(
                                          fontSize: 14, color: Colors.grey),
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      )
                    ])
                  ])),
            )
          ]),
        ));
  }
}
